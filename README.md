# Load balancer configuration
**Main configuration**
![Scheme](load-balancer-main.png)

**Instances configuration**
![Scheme](load-balancer-instances.png)

**Instances health check**
![Scheme](load-balancer-health-check.png)

# Load balancing
**Load data from server A**
![Scheme](load-balancer-server-a.png)

**Load data from server B**
![Scheme](load-balancer-server-b.png)
